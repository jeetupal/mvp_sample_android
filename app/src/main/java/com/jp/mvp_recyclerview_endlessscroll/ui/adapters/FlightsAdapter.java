package com.jp.mvp_recyclerview_endlessscroll.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jp.mvp_recyclerview_endlessscroll.R;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.Utility;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants;
import com.jp.mvp_recyclerview_endlessscroll.model.Fare;
import com.jp.mvp_recyclerview_endlessscroll.model.Flight;
import com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight.FlightDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by jeetupal on 30/03/17.
 */
public class FlightsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context context;
  private List<Flight> flightsList;
  private final int VIEW_TYPE_ITEM = 1;
  private final int VIEW_TYPE_PROGRESSBAR = 0;
  public FlightsAdapter(Context context, List<Flight> flightsList) {
    this.context = context;
    this.flightsList = flightsList;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view;
    RecyclerView.ViewHolder holder = null;
    if (viewType == VIEW_TYPE_ITEM) {
      view = layoutInflater.inflate(R.layout.list_item_flights, parent, false);
      holder = new FlightsViewHolder(view);
    } else {
      view = layoutInflater.inflate(R.layout.layout_progress_circle, parent, false);
      holder = new ProgressViewHolder(view);
    }

    return holder;
  }
  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Flight flightsPojo = flightsList.get(position);
    if (holder instanceof FlightsViewHolder) {
      FlightsViewHolder flightsViewHolder = (FlightsViewHolder) holder;
      flightsViewHolder.tvAirlineCode.setText( VariableConstants.AIRLINES_MAP.get(flightsPojo.getAirlineCode()) +"( " +flightsPojo.getAirlineCode() +" )" );
      flightsViewHolder.tvDepartureArrivalTime.setText( Utility.convertUnixTimeStampToDate((long) flightsPojo.getDepartureTime())+ " --> " + Utility.convertUnixTimeStampToDate((long) flightsPojo.getArrivalTime()) );
      flightsViewHolder.textViewClass.setText(flightsPojo.getClass_());
       CharSequence x = DateUtils.getRelativeTimeSpanString( (long) flightsPojo.getArrivalTime(), (long) flightsPojo.getDepartureTime(), 0);
      flightsViewHolder.tvDurationOrType.setText( x );
      List<Fare> fareList = flightsPojo.getFares();
      int [] array = new int[fareList.size()];
      for (int i = 0; i < fareList.size(); i++) {
        array[i] = fareList.get(i).getFare();
      }
      int minFarePrice = Utility.getMinValue(array);
      flightsViewHolder.tvFare.setText(context.getResources().getString(R.string.rupee) + minFarePrice);
    } else if (holder instanceof ProgressViewHolder) {
      ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
    }
  }
  @Override
  public int getItemCount() {
    if (flightsList != null) {
      return flightsList.size();
    }
    return 0;
  }
  @Override
  public int getItemViewType(int position) {
    return flightsList.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_PROGRESSBAR;
  }

  public class FlightsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    @BindView(R.id.imageView3) ImageView imageView3;
    @BindView(R.id.tv_airline_code) TextView tvAirlineCode;
    @BindView(R.id.tv_departure_arrival_time) TextView tvDepartureArrivalTime;
    @BindView(R.id.tv_duration_or_type) TextView tvDurationOrType;
    @BindView(R.id.tv_fare) TextView tvFare;
//    @BindView(R.id.textView10) TextView textView10;
    @BindView(R.id.historyCardView) CardView historyCardView;
    @BindView(R.id.tv_class) TextView textViewClass;
    @OnClick({R.id.historyCardView})
    public void onClick(View v) {
      Timber.d("item click");
      int postion = getLayoutPosition();
      Timber.d(""+postion);
      Intent intent = new Intent( context, FlightDetailActivity.class);
      Flight flight = flightsList.get(getLayoutPosition());
      intent.putExtra(VariableConstants.INTENT_FLIGHT_DATA, flight);
      context.startActivity( intent );
    }
    public FlightsViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, itemView);
    }
  }

  public class ProgressViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.progressBar) ProgressBar progressBar;
    public ProgressViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, itemView);
    }
  }
}
