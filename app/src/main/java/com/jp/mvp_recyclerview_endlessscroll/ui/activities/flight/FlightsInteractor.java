package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import com.jp.mvp_recyclerview_endlessscroll.model.Flights;

/**
 * Created by jeetupal on 30/03/17.
 */
public interface FlightsInteractor {
  interface GetFlightsCallbacks {
    void getFlightsCallbackSucess( Flights flights);
    void getFlightsCallbackFailure( String msg);
  }
  void getFlightApi( String url );
}
