package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jp.mvp_recyclerview_endlessscroll.BaseApplication;
import com.jp.mvp_recyclerview_endlessscroll.libs.network.StringRequestWrapper;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.Utility;
import com.jp.mvp_recyclerview_endlessscroll.model.Flights;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeetupal on 30/03/17.
 */
public class FlightsInteractorImp implements FlightsInteractor {
  GetFlightsCallbacks getFlightsCallbacks ;

  public FlightsInteractorImp(GetFlightsCallbacks getFlightsCallbacks) {
    this.getFlightsCallbacks = getFlightsCallbacks;
  }
  @Override
  public void getFlightApi(String url) {
    Map headerMap = new HashMap();
    Map paramsMap = new HashMap();
    StringRequestWrapper requestWrapper = new StringRequestWrapper(Request.Method.GET, url, paramsMap, headerMap, Request.Priority.NORMAL, new Response.Listener<String>() {
      @Override
      public void onResponse(String response) {
        if (!Utility.isNullOrEmpty(response)) {
          Utility.setProvidersList( response );
          Utility.setAirportsList( response );
          Utility.setAirlineList( response );
          Gson gson = new Gson();
          Flights flights = gson.fromJson(response, Flights.class);
          getFlightsCallbacks.getFlightsCallbackSucess(flights);
        } else {
          getFlightsCallbacks.getFlightsCallbackFailure(response);
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        getFlightsCallbacks.getFlightsCallbackFailure( error.getMessage()+"");
      }
    });

      BaseApplication.getInstance().getRequestQueue().add(requestWrapper);
  }
}
