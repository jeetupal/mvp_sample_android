package com.jp.mvp_recyclerview_endlessscroll.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.jp.mvp_recyclerview_endlessscroll.BaseApplication;
import com.squareup.leakcanary.RefWatcher;

/**
 * Created by jeetupal on 30/03/17.
 */
public class BaseActivity extends AppCompatActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    RefWatcher refWatcher = BaseApplication.getRefWatcher(this);
    refWatcher.watch(this);
  }
}
