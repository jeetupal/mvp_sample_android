package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import android.content.Context;

import com.jp.mvp_recyclerview_endlessscroll.R;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.Utility;
import com.jp.mvp_recyclerview_endlessscroll.model.Flight;
import com.jp.mvp_recyclerview_endlessscroll.model.Flights;

import java.util.List;

/**
 * Created by jeetupal on 30/03/17.
 */
public class FlightsPresenterImp implements FlightsPresenter, FlightsInteractorImp.GetFlightsCallbacks {

  FlightsView flightsView;
  FlightsInteractor flightsInteractor;

  public FlightsPresenterImp(FlightsView flightsView) {
    this.flightsView = flightsView;
    flightsInteractor = new FlightsInteractorImp(this);
  }
  @Override
  public void getFlightsInfo(String url) {
    if (Utility.isNetworkAvailable((Context) flightsView)){
      flightsView.showProgress();
      flightsInteractor.getFlightApi( url);
    }
  }
  @Override
  public void getFlightsCallbackSucess(Flights flights) {
    List<Flight> flightList = flights.getFlights();
    if ( flightList.size() == 0 ){
      flightsView.showToast(R.string.app_name);
    }
    flightsView.setToolBarData( flights );
    flightsView.setDataToAdapter( flights );
    flightsView.hideProgress();
  }
  @Override
  public void getFlightsCallbackFailure(String msg) {
    flightsView.hideProgress();
    flightsView.showToast( msg );
  }
}
