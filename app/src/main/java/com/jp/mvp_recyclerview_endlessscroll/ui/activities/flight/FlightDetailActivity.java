package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jp.mvp_recyclerview_endlessscroll.R;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.Utility;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants;
import com.jp.mvp_recyclerview_endlessscroll.model.Fare;
import com.jp.mvp_recyclerview_endlessscroll.model.Flight;
import com.jp.mvp_recyclerview_endlessscroll.ui.activities.BaseActivity;
import com.jp.mvp_recyclerview_endlessscroll.ui.widgets.Providers;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlightDetailActivity extends BaseActivity {
  @BindView(R.id.tv_toolbar_title) TextView tvToolbarTitle;
  @BindView(R.id.tv_toolbar_date) TextView tvToolbarDate;
  @BindView(R.id.progressBar) ProgressBar progressBar;
  @BindView(R.id.booking_options) TextView bookingOptions;
  @BindView(R.id.titile) TextView titile;
  @BindView(R.id.subtitile) TextView subtitile;
  @BindView(R.id.imageView) ImageView imageView;
  @BindView(R.id.textView11) TextView textViewDepartureInfo;
  @BindView(R.id.textView12) TextView textViewArrivalInfo;
  @BindView(R.id.textView13) TextView tvDepartureAirport;
  @BindView(R.id.tv_dep_airport) TextView tvArrivalAirport;
  @BindView(R.id.scrollView) ScrollView scrollView;
  @BindView(R.id.activity_flight_detail) RelativeLayout activityFlightDetail;
  Flight flight;
  @BindView(R.id.tv_final_fare) TextView tvFinalFare;
  @BindView(R.id.appbarlayout) AppBarLayout appbarlayout;
  @BindView(R.id.flightImg) ImageView flightImg;
  @BindView(R.id.buttons) LinearLayout buttons;
  @BindView(R.id.parent_top_ll) LinearLayout parentTopLinearLayout;
  Context context;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_flight_detail);
    ButterKnife.bind(this);
    if (getIntent().getExtras() != null) {
      flight = getIntent().getParcelableExtra(VariableConstants.INTENT_FLIGHT_DATA);
    }
    initialiseView();
  }
  private void initialiseView() {
    tvToolbarTitle.setText(VariableConstants.AIRPORTS_MAP.get(flight.getOriginCode()) + " -- " + VariableConstants.AIRPORTS_MAP.get(flight
      .getDestinationCode()));
    List<Fare> fareList = flight.getFares();
    /**
     * Dynamically create providrers widget
     * @see Providers
     */
    ViewGroup viewGroup = (ViewGroup) parentTopLinearLayout;
    for (int k = 0; k < fareList.size(); k++) {
      Providers providers = new Providers(this);
      providers.setTvProviderName("" + VariableConstants.PROVIDERS_MAP.get(fareList.get(k).getProviderId()));
      providers.setTvFare(getResources().getString(R.string.rupee) + "" + fareList.get(k).getFare());
      parentTopLinearLayout.addView(providers);
    }
    titile.setText(VariableConstants.AIRPORTS_MAP.get(flight.getOriginCode()) + " -- " + VariableConstants.AIRPORTS_MAP.get(flight
      .getDestinationCode()));
    subtitile.setText(VariableConstants.AIRLINES_MAP.get(flight.getAirlineCode()) + " " + flight.getAirlineCode() + " - " + flight
      .getClass_());
    textViewDepartureInfo.setText(flight.getOriginCode() + " " + Utility.convertUnixTimeStampToDate((long) flight.getDepartureTime()));
    textViewArrivalInfo.setText(flight.getDestinationCode() + " " + Utility.convertUnixTimeStampToDate((long) flight.getArrivalTime()));
    tvDepartureAirport.setText(Utility.convertUnixTimeStampToDate((long) flight.getArrivalTime()));
    tvArrivalAirport.setText(Utility.convertUnixTimeStampToDate((long) flight.getDepartureTime()));
    tvFinalFare.setText(getResources().getString(R.string.rupee) + "" + fareList.get(0).getFare());
  }
}
