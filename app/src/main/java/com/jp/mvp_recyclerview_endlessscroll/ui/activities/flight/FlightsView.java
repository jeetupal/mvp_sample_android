package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import com.jp.mvp_recyclerview_endlessscroll.model.Flights;

/**
 * Created by jeetupal on 30/03/17.
 */
public interface FlightsView {

  void showProgress();
  void hideProgress();
  void showToast(int msg);
  void showToast(String message);
  void setDataToAdapter( Flights flights);
  void setToolBarData( Flights flights);
}
