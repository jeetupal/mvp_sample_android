package com.jp.mvp_recyclerview_endlessscroll.ui.widgets;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jp.mvp_recyclerview_endlessscroll.R;

/**
 * Created by jeetupal on 31/03/17.
 */

public class Providers extends LinearLayout {
  Context context;
  Activity activity;
  TextView tvFare;
  Button btnBook1;
  TextView tvProvider1;
  RelativeLayout providerDetail;
  LinearLayout providerLl;

  public Providers(Context context) {
    super(context);
    this.context = context;
    this.activity = (Activity) context;
    init();
  }
  public Providers(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    init();
  }
  private void init() {
    inflate(getContext(), R.layout.widget_providers_item, this);
    this.tvProvider1 = (TextView) findViewById(R.id.tv_provider1);
    this.tvFare = (TextView) findViewById(R.id.tv_fare);
  }
  /**
   * @param fare
   */
  public void setTvFare(String fare){
    this.tvFare.setText( fare);
  }
  /**
   * @param providerName
   */
  public void setTvProviderName(String providerName) {
   this.tvProvider1.setText( providerName);
  }
}
