package com.jp.mvp_recyclerview_endlessscroll.ui.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;
import com.jp.mvp_recyclerview_endlessscroll.R;
import com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight.FlightsActivity;
/**
 * Created by jeetupal on 31/03/17.
 */
public class SplashActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    try {
      VideoView videoView = new VideoView(this);
      setContentView(videoView);
      Uri uriPath = Uri.parse("android.resource://"+getPackageName() + "/"+ R.raw.splash_video);
      videoView.setVideoURI( uriPath );
      videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
          navigateToHomeActivity();
        }
      });
      videoView.start();
    } catch (Exception e) {
      e.printStackTrace();
      navigateToHomeActivity();
    }
  }
  private void navigateToHomeActivity() {
    if (isFinishing()){
      return;
    }
    this.startActivity(new Intent(this, FlightsActivity.class));
    finish();
  }
}
