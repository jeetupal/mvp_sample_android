package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jp.mvp_recyclerview_endlessscroll.R;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.ApiHelper;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.Utility;
import com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants;
import com.jp.mvp_recyclerview_endlessscroll.model.Fare;
import com.jp.mvp_recyclerview_endlessscroll.model.Flight;
import com.jp.mvp_recyclerview_endlessscroll.model.Flights;
import com.jp.mvp_recyclerview_endlessscroll.ui.activities.BaseActivity;
import com.jp.mvp_recyclerview_endlessscroll.ui.adapters.FlightsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlightsActivity extends BaseActivity implements FlightsView {

  @BindView(R.id.flight_recyclerView) RecyclerView flightRecyclerView;
  FlightsPresenter flightsPresenter;
  FlightsAdapter flightsAdapter;
  LinearLayoutManager linearLayoutManager;
  List<Flight> flightsList;
  @BindView(R.id.bottom_nav) BottomNavigationView bottomNav;
  @BindView(R.id.tv_toolbar_title) TextView tvToolbarTitle;
  @BindView(R.id.tv_toolbar_date) TextView tvToolbarDate;
  @BindView(R.id.toolbar) AppBarLayout toolbar;
  @BindView(R.id.progressBar) ProgressBar progressBar;
  private boolean exit = false;
  //  @BindView(R.id.AppBarLayout) android.support.design.widget.AppBarLayout AppBarLayout;
  //  @BindView(R.id.bottomSheet) NestedScrollView bottomSheet;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    initialiseViewAndListner();
    flightsPresenter = new FlightsPresenterImp(this);
    flightsPresenter.getFlightsInfo(ApiHelper.getFlightsLink(this));
  }

  private void initialiseViewAndListner() {
    flightsList = new ArrayList<>();
    flightRecyclerView.setHasFixedSize(true);
    linearLayoutManager = new LinearLayoutManager(this);
    flightRecyclerView.setLayoutManager(linearLayoutManager);
    flightsAdapter = new FlightsAdapter(this, flightsList);
    flightRecyclerView.setAdapter(flightsAdapter);
    bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
          case R.id.early: {
            Collections.sort(flightsList, new Comparator<Flight>() {
              @Override
              public int compare(Flight o1, Flight o2) {
                return Double.compare(o1.getDepartureTime(), o2.getDepartureTime());
              }
            });
            flightsAdapter.notifyDataSetChanged();
            break;
          }
          case R.id.cheap: {
            Collections.sort(flightsList, new Comparator<Flight>() {
              @Override
              public int compare(Flight o1, Flight o2) {
                return o1.getMinFare().compareTo(o2.getMinFare());
              }
            });
            flightsAdapter.notifyDataSetChanged();
            break;
          }
          case R.id.type: {
            Collections.sort(flightsList, new Comparator<Flight>() {
              @Override
              public int compare(Flight o1, Flight o2) {
                return Double.compare(o1.getArrivalTime(), o2.getArrivalTime());
              }
            });
            flightsAdapter.notifyDataSetChanged();
            break;
          }
        }
        return true;
      }
    });

  }

  @Override
  public void showProgress() {
    progressBar.setVisibility(View.VISIBLE);
    flightRecyclerView.setVisibility(View.GONE);
    bottomNav.setVisibility(View.GONE);
  }
  @Override
  public void hideProgress() {
    progressBar.setVisibility(View.GONE);
    flightRecyclerView.setVisibility(View.VISIBLE);
    bottomNav.setVisibility(View.VISIBLE);
  }
  @Override
  public void showToast(int msg) {
    Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();
  }
  @Override
  public void showToast(String msg) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
  }
  @Override
  public void setDataToAdapter(Flights flights) {

    /**
     * Since given data set sucks.
     *  custom : set min.fare for each flight.
     */
    List<Flight> flightList = flights.getFlights();
    for (int j = 0; j < flightList.size(); j++) {
      List<Fare> fareList = flightList.get(j).getFares();
      int[] array = new int[fareList.size()];
      for (int i = 0; i < fareList.size(); i++) {
        array[i] = fareList.get(i).getFare();
      }
      int minFarePrice = Utility.getMinValue(array);
      flightList.get(j).setMinFare(minFarePrice);
    }

    flightsList.addAll(flightList);
    flightsAdapter.notifyDataSetChanged();
  }
  @Override
  public void setToolBarData(Flights flights) {
    tvToolbarTitle.setText(VariableConstants.AIRPORTS_MAP.get(flights.getFlights().get(0).getOriginCode())+ " ----> " + VariableConstants.AIRPORTS_MAP.get(flights.getFlights().get(0).getDestinationCode()));
    tvToolbarDate.setText("");
  }

  @Override
  public void onBackPressed() {
//    super.onBackPressed();
    if (exit) {
      finish();
    } else {
      exit = true;
      Toast.makeText(this, getString(R.string.press_back_exit), Toast.LENGTH_SHORT).show();
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          exit = false;
        }
      }, 3 * 1000);
    }
  }
}
