package com.jp.mvp_recyclerview_endlessscroll.ui.activities.flight;

/**
 * Created by jeetupal on 30/03/17.
 */
public interface FlightsPresenter {
  void getFlightsInfo(String url);
}
