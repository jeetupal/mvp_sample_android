package com.jp.mvp_recyclerview_endlessscroll.libs.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import static com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants.AIRLINES_MAP;
import static com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants.AIRPORTS_MAP;
import static com.jp.mvp_recyclerview_endlessscroll.libs.utils.VariableConstants.PROVIDERS_MAP;

/**
 * Created by jeetupal on 30/03/17.
 */

public class Utility {
  /**
   *  Check network connection.
   * @param context
   */
  public static boolean isNetworkAvailable(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
  }

  /**
   *  Check if null or empty string.
   * @param str
   */
  public static boolean isNullOrEmpty(String str) {
    return (str.equals(null) || str.equals("") || str.isEmpty());
  }

  /**
   * @param array
   */
  public static int getMinValue( int [] array){
    int minValue = array[0];
    for (int i = 0; i < array.length; i++) {
      if ( array[0] < minValue){
        minValue = array[i];
      }
    }
    return minValue;
  }

  /**
   *  @param unixTimestampInMili
   */
  public static String convertUnixTimeStampToDate( long unixTimestampInMili){
    String result = null;
    Date date = new Date( unixTimestampInMili );
//    yyyy-MM-dd HH:mm:ss z
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4"));
    result = simpleDateFormat.format(date);
    return result;
  }

  /**
   *  Convert JsonObject into StringArray
   *  set flight providers list.
   *  @param response
   */
  public static void setProvidersList(String response){
    ArrayList<String> stringArrayList=new ArrayList<String>();
    JSONObject obj= null;
    JSONObject jsonObjectProviders = null;
    try {
      obj = new JSONObject(response);
      jsonObjectProviders = obj.getJSONObject("appendix").getJSONObject("providers");
      Iterator iter = jsonObjectProviders.keys();
      while(iter.hasNext()){
        String key = (String)iter.next();
        stringArrayList.add(jsonObjectProviders.getString(key));
        PROVIDERS_MAP.put( Integer.parseInt(key) , jsonObjectProviders.getString(key));
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  /**
   *  set Airports list.
   *  @param response
   */
  public static void setAirportsList(String response) {
    ArrayList<String> stringArrayList = new ArrayList<String>();
    JSONObject jsonObject = null;
    try {
      jsonObject = new JSONObject(response);
      JSONObject jsonObjectAirports = jsonObject.getJSONObject("appendix").getJSONObject("airports");
      Iterator iterator = jsonObjectAirports.keys();
      while (iterator.hasNext()){
        String key = (String) iterator.next();
        stringArrayList.add(jsonObjectAirports.getString(key));
        AIRPORTS_MAP.put( key, jsonObjectAirports.getString(key));
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  /**
   *  set Airlines list.
   *  @param response
   */
  public static void setAirlineList(String response){
    ArrayList<String> stringArrayList = new ArrayList<String>();
    try {
      JSONObject jsonObject = new JSONObject(response);
      JSONObject jsonObjectAirlines = jsonObject.getJSONObject("appendix").getJSONObject("airlines");
      Iterator iterator = jsonObjectAirlines.keys();
      while ( iterator.hasNext()){
        String key = (String) iterator.next();
        stringArrayList.add(jsonObjectAirlines.getString( key));
        AIRLINES_MAP.put( key , jsonObjectAirlines.getString(key));
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }
}
