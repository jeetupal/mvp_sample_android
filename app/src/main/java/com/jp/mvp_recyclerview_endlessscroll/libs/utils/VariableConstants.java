package com.jp.mvp_recyclerview_endlessscroll.libs.utils;

import java.util.HashMap;

/**
 * Created by jeetupal on 30/03/17.
 */

public class VariableConstants {
  public static final String INTENT_FLIGHT_DATA = "fligt_data";
  public static final HashMap<Integer, String> PROVIDERS_MAP = new HashMap<>();
  public static final HashMap<String, String> AIRPORTS_MAP = new HashMap<>();
  public static final HashMap<String, String> AIRLINES_MAP = new HashMap<>();
//  public static final SparseArray<String> PROVIDERS_MAPP = new SparseArray<String>();
}
