package com.jp.mvp_recyclerview_endlessscroll;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import timber.log.Timber;

/**
 * Created by jeetupal on 30/03/17.
 */

public class BaseApplication extends MultiDexApplication {
    public static final String TAG = BaseApplication.class.getName();
    private RequestQueue mRequestQueue;
    private static BaseApplication mInstance;
    private RefWatcher refWatcher;

    /**
     *  @param context
     */
    public static RefWatcher getRefWatcher( Context context){
        BaseApplication baseApplication = (BaseApplication) context.getApplicationContext();
        return baseApplication.refWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        leakcanary installation
        refWatcher = LeakCanary.install(this);
//        volley setup
        mInstance = this;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        Timber plant insetion
        if (BuildConfig.DEBUG){
            Timber.plant( new Timber.DebugTree());
        } else {
//      plant a tree for release falvour
        }
    }

    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public <T> void add(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancel() {
        mRequestQueue.cancelAll(TAG);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
