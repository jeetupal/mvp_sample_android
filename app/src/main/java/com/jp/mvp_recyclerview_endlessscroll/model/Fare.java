
package com.jp.mvp_recyclerview_endlessscroll.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fare implements Parcelable {

    @SerializedName("providerId")
    @Expose
    private Integer providerId;
    @SerializedName("fare")
    @Expose
    private Integer fare;

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getFare() {
        return fare;
    }

    public void setFare(Integer fare) {
        this.fare = fare;
    }

    @Override
    public int describeContents() { return 0; }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.providerId);
        dest.writeValue(this.fare);
    }
    public Fare() {}
    protected Fare(Parcel in) {
        this.providerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fare = (Integer) in.readValue(Integer.class.getClassLoader());
    }
    public static final Parcelable.Creator<Fare> CREATOR = new Parcelable.Creator<Fare>() {
        @Override
        public Fare createFromParcel(Parcel source) {return new Fare(source);}
        @Override
        public Fare[] newArray(int size) {return new Fare[size];}
    };
}
