
package com.jp.mvp_recyclerview_endlessscroll.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Flight implements Parcelable {

    @SerializedName("originCode")
    @Expose
    private String originCode;
    @SerializedName("destinationCode")
    @Expose
    private String destinationCode;
    @SerializedName("departureTime")
    @Expose
    private double departureTime;
    @SerializedName("arrivalTime")
    @Expose
    private double arrivalTime;
    @SerializedName("fares")
    @Expose
    private List<Fare> fares = null;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("class")
    @Expose
    private String _class;

    private Integer minFare;
    public Integer getMinFare() {
        return minFare;
    }
    public void setMinFare(Integer minFare) {
        this.minFare = minFare;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(double departureTime) {
        this.departureTime = departureTime;
    }

    public double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    @Override
    public int describeContents() { return 0; }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.originCode);
        dest.writeString(this.destinationCode);
        dest.writeDouble(this.departureTime);
        dest.writeDouble(this.arrivalTime);
        dest.writeTypedList(this.fares);
        dest.writeString(this.airlineCode);
        dest.writeString(this._class);
        dest.writeValue(this.minFare);
    }
    public Flight() {}
    protected Flight(Parcel in) {
        this.originCode = in.readString();
        this.destinationCode = in.readString();
        this.departureTime = in.readDouble();
        this.arrivalTime = in.readDouble();
        this.fares = in.createTypedArrayList(Fare.CREATOR);
        this.airlineCode = in.readString();
        this._class = in.readString();
        this.minFare = (Integer) in.readValue(Integer.class.getClassLoader());
    }
    public static final Parcelable.Creator<Flight> CREATOR = new Parcelable.Creator<Flight>() {
        @Override
        public Flight createFromParcel(Parcel source) {return new Flight(source);}
        @Override
        public Flight[] newArray(int size) {return new Flight[size];}
    };
}
